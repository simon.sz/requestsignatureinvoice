package com.strautmann.hungaria;

import org.apache.commons.codec.digest.DigestUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

public class Main {

    public static void main(String[] args) {


        int args_lenth = 0;

        if(args.length > 0){
            args_lenth = args.length;
        }

        switch (args_lenth){
            case 0 :
                System.out.println("Túl kevés paraméter!");break;
            case 1 :
                System.out.println("Túl kevés paraméter"); break;
            case 2 :
                System.out.println("Túl kevés paraméter"); break;
            case 3 :
                getRequestsignatureInvoice(args[0],args[1],args[2]);
                break;
            default :
                System.out.println("Túl sok paraméter értéket adott meg"); break;
        }






    }


    public static String getRequestsignatureInvoice(String requestId,String xmlSignkey,String invoice){
        String requestSignature = DigestUtils.sha512Hex(requestId+ currentTimeStamp()+xmlSignkey+crc32(invoice)).toUpperCase();
        System.out.println("RequestsignatureInvoice éréték:\n" + requestSignature);
        return requestSignature;
    }


    public static String  crc32(String invoice){
        String input = invoice;
        // get bytes from string
        byte bytes[] = input.getBytes();

        Checksum checksum = new CRC32();

        // update the current checksum with the specified array of bytes

        checksum.update(bytes, 0, bytes.length);

        // get the current checksum value

        long checksumValue = checksum.getValue();
        System.out.println("CRC32 checksum for input string is: " + checksumValue);
        return Long.toString(checksumValue);

    }

    public static String currentTimeStamp(){
        //Europe/Budapest
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        Date timestamp = calendar.getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        df.setTimeZone(timeZone);
        String timestampStr = df.format(timestamp);
        return timestampStr;
    }
}
